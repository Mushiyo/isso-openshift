#!/usr/bin/python
import os
import site
site.addsitedir(os.environ['OPENSHIFT_PYTHON_DIR'] + '/virtenv/')
from isso import make_app, config
application = make_app(config.load(os.environ['OPENSHIFT_REPO_DIR'] + '/share/isso.conf'))
